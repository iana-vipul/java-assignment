As per my commitment, I have completed all task given by you. I took total 8 hours to complete the assignment work.

Following changes have been added to SVN. I have completed following remaining two challenges and all code changes have been completed.

1) fix_dao_tests: Fix all test cases which were failed due to incorrect code, fixed all code.

2) service_layout_tests: Fix all test cases in service layers which were failed due to incorrect code, fixed all code.

* Time taken to fixed above issues: 5.5 hours.
* Time taken to complete all task: 8 hours.

Please let me know if you need more details how did I fix the problem. 