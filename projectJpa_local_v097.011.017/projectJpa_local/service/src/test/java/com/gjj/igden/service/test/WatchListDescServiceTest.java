package com.gjj.igden.service.test;

import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gjj.igden.model.Account;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.watchlist.WatchListDescService;

@Configuration
@ComponentScan(basePackageClasses = {WatchListDescService.class, JPATestConfig.class})
class SpringTextContext {
}

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringTextContext.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WatchListDescServiceTest {
  @Autowired
  private WatchListDescService watchListDescService;

  @Test
  public void B_simpleReadTest() throws Exception {
    watchListDescService.getStockSymbolsList(1L).forEach(System.out::println);
  }

  @Test
  public void A_createH2DataBaseTest() {
    List<WatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmount = 4;
    Assert.assertEquals(expectedDataSetsAmount, dataSetList.size());
  }

  @Test
  public void E_ReturnBarList() {
    IWatchListDesc dataSet = watchListDescService.getDataSetsAttachedToAcc(2L).get(0);
    System.out.println(dataSet.getWatchListName());
    Assert.assertNotNull(dataSet);
    Assert.assertEquals("test-aapl-5minBar-preMarketdata", dataSet.getWatchListName());
  }

  @Test
  public void G_testDelete02() throws Exception {
    List<WatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmount = dataSetList.size();

    boolean deleteResultFlag = watchListDescService.delete(dataSetList.get(0).getWatchListId(),dataSetList.get(0).getAccount().getId());
    Assert.assertTrue(deleteResultFlag);
    System.out.println("after deletion ");
    dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    Assert.assertNotEquals(expectedDataSetsAmount, dataSetList.size());
  }

  @Test
  public void F_testCreateDataSet() throws Exception {
    Long accId = 1L;
    IWatchListDesc newWatchList = watchListDescService.getWatchListDesc(2L, accId);
    List<WatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    int expectedDataSetsAmountAfterDeletion = 4;
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
    Assert.assertNotNull(newWatchList);
    IWatchListDesc dummyWatchListDesc =  newWatchList;
      dummyWatchListDesc.setId(111L);
    Account account = new Account();
    account.setId(accId);
    ((WatchListDesc)dummyWatchListDesc).setAccount(account);
      dummyWatchListDesc.setWatchListName("just testing around");
    Assert.assertTrue(watchListDescService.create((WatchListDesc) dummyWatchListDesc, accId));
    dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    expectedDataSetsAmountAfterDeletion = 5;
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
  }

  @Test
  public void C_Update() throws Exception {
    final Long accId = 1L;
    IWatchListDesc dataSet = watchListDescService.getWatchListDesc(5L, accId);
    dataSet.setWatchListName("test update");
    Account account = new Account();
    account.setId(accId);
    ((WatchListDesc)dataSet).setAccount(account);
    watchListDescService.update(dataSet);
    final String dataSetNameDirect = watchListDescService.getWatchListDesc(5L, 1L).getWatchListName();
    Assert.assertEquals("test update", dataSetNameDirect);
  }

  @Test
  public void D_Read() throws Exception {
    List<WatchListDesc> watchListDescs = watchListDescService.getDataSetsAttachedToAcc(1L);
    final int size = 4;
    Assert.assertEquals(size, watchListDescs.size());
  }

}
