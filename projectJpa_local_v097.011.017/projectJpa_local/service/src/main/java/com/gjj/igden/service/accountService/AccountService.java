package com.gjj.igden.service.accountService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.gjj.igden.dao.AccountDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.dao.daoimpl.AccountDaoImpl;
import com.gjj.igden.dao.daoimpl.WatchListDescDaoImpl;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.passwordencoder.AppPasswordEncoder;

@Service
@Transactional(propagation=Propagation.REQUIRED)
public class AccountService {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
	
  @Autowired
  private AccountDaoImpl accountDaoImpl;
  
  @Autowired
  private AppPasswordEncoder appPasswordEncoder;
  
  @Autowired
  private WatchListDescDaoImpl watchListDescDao;
  
  public List<Account> getAccountList() {
	  return accountDaoImpl.readAll();
  }

  public boolean createAccount(Account account) throws IOException, DAOException {
	  
	  try {
		  account.setCreationDate(new Date());
		  account.setEnabled(true);
		  account.setPassword(AppPasswordEncoder.generatePassword(account.getPassword(), account.getSaltSource()));
		  accountDaoImpl.create(account);
		  logger.debug("Creating account in account service::" + account);
		  return true;
		} catch (DAOException e) {
			logger.error("Account not created" + account);
			throw new RuntimeException("Account not created::", e.getCause());
		}
  }

  public boolean updateAccount(Account account) {
	  Account ac = getAccount(account.getId());
	  System.out.println("AccountService.updateAccount():::::"+ac);
	  logger.debug("Upadating account in account service::" + account);
	  if(null != ac) {
		  ac.setAccountName(account.getAccountName());
		  ac.setAdditionalInfo(account.getAdditionalInfo());
		  ac.setEmail(account.getEmail());
	  }
	  accountDaoImpl.update(ac);
		return true;
  }
  
  public Account getAccount(Long accId) {
	  logger.debug("Retrieving account in account service for id::" + accId);
	  return accountDaoImpl.read(new Account(accId));
  }

  public Account retrieveAccount(Long accId) {
	  logger.debug("Retrieving account with datasets in account service for id::" + accId);
	  Account account = new Account();
	  account = accountDaoImpl.read(new Account(accId));
	  List<WatchListDesc> dataSets = watchListDescDao.getDataSetsAttachedToAcc(accId);
	  account.setDataSets(dataSets);
	  
	  return account;
  }

  public boolean delete(Long id) throws DAOException {
	  logger.debug("Deleting account in account service for id::" + id);	
	  Account account = getAccount(id);
	  accountDaoImpl.delete(account);	
	  return false;
  }

  public boolean setImage(long accId, InputStream is) throws IOException {
	  logger.debug("Setting image in account service for id::" + accId);
    return accountDaoImpl.setImage(accId, is);
  }

  public byte[] getImage(int accId){
	  logger.debug("Retrieving image in account service for id::" + accId);
    return accountDaoImpl.getImage(accId);
  }
  
  public boolean validateExtension(String fileName) {
	  if(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg") || fileName.endsWith(".png"))
		  return true;
	  return false;
  }
  
  public boolean validateExtensionForXml(String fileName) {
	  if(fileName.endsWith(".xml"))
		  return true;
	  return false;
  }
  
  public String parseToXML(Account account) throws Exception {
	    StringWriter sw = new StringWriter();
	    try {
	      JAXBContext context = JAXBContext.newInstance(com.gjj.igden.xml.model.Account.class);
	      Marshaller marshaller = context.createMarshaller();
	      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	      marshaller.marshal(new com.gjj.igden.xml.model.Account(account), sw);
	    } catch (JAXBException e) {
	    	throw new Exception(e.getMessage(), e);
	    }
	    return sw.toString();
	  }

	  public com.gjj.igden.xml.model.Account parseFromXML(File file) throws Exception {
		  com.gjj.igden.xml.model.Account account;
	    try {
	    	System.out.println(System.getProperty("java.io.tmpdir"));
	     // file = new File(System.getProperty("java.io.tmpdir") + "/" + file.getPath());
	      JAXBContext jc = JAXBContext.newInstance(com.gjj.igden.xml.model.Account.class);
	      Unmarshaller unmarshaller = jc.createUnmarshaller();
	      account = (com.gjj.igden.xml.model.Account) unmarshaller.unmarshal(file);
	    } catch (JAXBException e) {
	      throw new Exception(e.getMessage(), e);
	    }
	    return account;
	  }
  
}
