package com.gjj.igden.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.activation.MimeType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.dao.daoimpl.AccountDaoImpl;
import com.gjj.igden.dao.daoimpl.RoleDaoImpl;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.Role;
import com.gjj.igden.service.accountService.AccountService;

@Controller
public class AccountController {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private AccountService service;

	@Autowired
	private RoleDaoImpl roleDaoImpl;

	@GetMapping("/admin/list-accounts")
	public String showAccounts(ModelMap model) {
		logger.debug("Reteriving list of Account");
		model.addAttribute("ACCOUNT_LIST", service.getAccountList());
		return "list-accounts";
	}

	@PostMapping("/admin/edit-account")
	public String putEditedAccountToDb(ModelMap model, Account account, RedirectAttributes redirectAttributes) {
		logger.debug("Editing Account for id::" + account.getId());
		try {
			boolean editStatus = service.updateAccount(account);
			if (editStatus) {
				redirectAttributes.addAttribute("success", "true");
			} else {
				System.err.println("editing failed");
				model.addAttribute("error", "File type is not supported");
			}
			return "redirect:/admin/list-accounts";
		} catch (Exception e) {
			model.addAttribute("error", "error occurred while updating Account!");
			model.addAttribute("account", account);
			return "edit-account";
		}
	}

	@GetMapping("/add-account")
	public String createNewAccountGet(ModelMap model) {
		logger.debug("Creating an Account");
		model.addAttribute("accountRoles", roleDaoImpl.readAll());
		model.addAttribute("account", new Account());
		return "add-account";
	}

	@PostMapping("/processAddAccount1")
	public String createAccountPost1(@RequestParam(value = "username1", required = false) String username1) {
		logger.debug("Saving username ::in createAccountPost1::" + username1);
		Account account = new Account();
		account.setAccountName(username1);
		try {
			service.createAccount(account);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return "redirect:/list-accounts";
	}

	@PostMapping("/processAddAccount")
	public String createAccountPost2(@RequestParam(value = "username1", required = false) String username1) {
		logger.debug("Saving username::in createAccountPost2::" + username1);
		Account account = new Account();
		account.setAccountName(username1);
		try {
			service.createAccount(account);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return "redirect:/list-accounts";
	}

	@PostMapping("/add-account")
	public String createAccountPost(Account account, @RequestParam(value = "role", required = false) String role) {
		logger.debug("Saving role and account::in createAccountPost::" + account);
		account.addRole(roleDaoImpl.read(new Role(Long.parseLong(role))));
		try {
			service.createAccount(account);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return "redirect:/admin/list-accounts";
	}

	@GetMapping("/delete-account")
	public String deleteAccount(@RequestParam Long id) {
		logger.debug("Deleting account for id::" + id);
		try {
			 service.delete(id);
		} catch (Exception e) {
			logger.error("Error while deleting account for id::" + id + e);
		}
			return "redirect:/admin/list-accounts";
	}

	@GetMapping("/admin/edit-account")
	public String getAccountToEditAndPopulateForm(ModelMap model, @RequestParam Long id) {
		logger.debug("Editing account for id:", + id);	
		Account account = service.retrieveAccount(id);
		logger.debug(String.format("Retrieving account for id::"),account);
		model.addAttribute("account", account);
		return "edit-account";
	}

	@GetMapping("/admin/getImage")
	@ResponseBody
	public void showImage(@RequestParam("accId") int accId , HttpServletResponse response )
			throws ServletException, IOException {
		logger.debug("Retrieving image for itemid",accId);
		byte[] buffer = service.getImage(accId);
		response.setContentType("image/jpg");
	    response.getOutputStream().write(buffer, 0, buffer.length);
	    response.getOutputStream();
	}

	@PostMapping("/admin/uploadImage") // new annotation since 4.3 todo make this new annotation everywhere
	public String setNewImage(ModelMap model, @RequestParam("image") MultipartFile imageFile, RedirectAttributes redirectAttributes,
			Account account) {
		Account account1 = service.retrieveAccount(account.getId());
		logger.debug("Uploading image for account" + account);
		if (imageFile.isEmpty()) {
			model.addAttribute("watchLists", account1.getAttachedWatchedLists());
			model.addAttribute("account", account1);
			model.addAttribute("error", "Please select file to upload'" + imageFile.getOriginalFilename() + "'");
			return "view-account";
		}
		
		if(!service.validateExtension(imageFile.getOriginalFilename())) {
			 model.addAttribute("error", "File type is not supported");
			 model.addAttribute("account", account1);
			 return "view-account";
		}
		try {
			byte[] bytes = imageFile.getBytes();
			InputStream imageConvertedToInputStream = new ByteArrayInputStream(bytes);
			service.setImage(account.getId(), imageConvertedToInputStream);
			model.addAttribute("watchLists", account1.getAttachedWatchedLists());
			model.addAttribute("account", account1);
			 model.addAttribute("message", "You successfully uploaded '" + imageFile.getOriginalFilename() + "'");
			/*redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + imageFile.getOriginalFilename() + "'");*/
			logger.debug("Image uploaded for account" + account);
		} catch (IOException e) {
			logger.error("Error in Image uploading for account" + account);
			e.printStackTrace();
		}
		
	        return "view-account";
	}

	@GetMapping("/uploadStatus")
	public String uploadStatus() {
		logger.debug("Image uploaded successfully");
		return "uploadStatus";
	}

	@GetMapping("/admin/view-account")
	public String viewAccount(ModelMap model, @RequestParam Long id) {
		logger.debug("Viewing account for id" + id);
		Account account = service.retrieveAccount(id);
		model.addAttribute("watchLists", account.getAttachedWatchedLists());
		model.addAttribute("account", account);
		return "view-account";
	}

	@RequestMapping(value = "/home")
	public String home() {
		logger.debug("Redirecting to home page");
		return "/other-jsp/home";
	}

	@RequestMapping(value = "/location")
	public String location() {
		logger.debug("Redirecting to location");
		return "/other-jsp/location";
	}
	
	@GetMapping("/admin/exportToXml")
	public void exportToXML(@RequestParam Long id, HttpServletResponse response) throws ServletException {
		try {
	        String mimeType = null;
	        if (mimeType == null) {
	            mimeType = "application/octet-stream";
	        }
	        response.setContentType(mimeType);
			Account account = service.retrieveAccount(id);
			String returnXMLFile = service.parseToXML(account);
			
			InputStream inputStream = new ByteArrayInputStream(returnXMLFile.getBytes(StandardCharsets.UTF_8.name()));
	        response.setHeader("Content-Disposition","attachment; filename=account.xml");
	        try {
	            int c;
	            while ((c = inputStream.read()) != -1) {
	            response.getWriter().write(c);
	            }
	        } finally {
	            if (inputStream != null) 
	                inputStream.close();
	                response.getWriter().close();
	        }
		} catch (Exception e) {
			throw new ServletException(e.getMessage(), e);
		} finally {
	        
		}
 
        
	}

	@PostMapping("/admin/uploadAccountXml")
	protected String uploadAccountXML(ModelMap model, @RequestParam MultipartFile file,
			RedirectAttributes redirectAttributes, @RequestParam Long id) throws ServletException {
		System.out.println("AccountController.uploadAccountXML()");
		Account account = service.retrieveAccount(id);
		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			model.addAttribute("watchLists", account.getAttachedWatchedLists());
			model.addAttribute("account", account);
			model.addAttribute("error", "Please select file to upload");
			return "view-account";
		}
		try {
			com.gjj.igden.xml.model.Account ac = service.parseFromXML(convert(file));
			account.setAccountName(ac.getAccountName());
			account.setEmail(ac.getEmail());
			account.setAdditionalInfo(ac.getAdditionalInfo());
		} catch (Exception e) {
			throw new ServletException(e.getMessage(), e);
		}
		model.addAttribute("account", account);
		return "edit-account";
	}

	public File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

}
