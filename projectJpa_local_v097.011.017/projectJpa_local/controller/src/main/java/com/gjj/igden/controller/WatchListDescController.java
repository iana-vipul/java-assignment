package com.gjj.igden.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.model.WatchListDescKey;
import com.gjj.igden.service.accountService.AccountService;
import com.gjj.igden.service.watchlist.WatchListDescService;

@Controller
public class WatchListDescController {
	
	private static final Logger logger = LoggerFactory.getLogger(GreetingController.class);
	
  @Autowired
  private WatchListDescService service;//renamed WatchListDescServiceService to WatchListDescService 
  
  @Autowired
  private AccountService accountService;
  
  @GetMapping("/admin/view-watchlist")
  public String viewWatchList(ModelMap model, @RequestParam Long watchListId) {
	logger.debug("Viewing watch-list for watchlist id::" + watchListId);
    model.addAttribute("stockSymbolsList", service.getStockSymbolsList(watchListId));
    model.addAttribute("watchListId", watchListId);
    return "view-watchlist";
  }

  @GetMapping(value = "/admin/add-watchlist")
  public String addWatchList(ModelMap model, @RequestParam Long id) {
	logger.debug("Adding watch-list for watchlist id::" + id);
    WatchListDesc theWatchListDesc = new WatchListDesc();
    theWatchListDesc.setWatchListDescKey(new WatchListDescKey(accountService.getAccount(id)));
    model.addAttribute("theWatchListDesc", theWatchListDesc);
    return "lazyRowLoad";
  }

  @PostMapping(value = "/admin/lazyRowAdd.web")
  public String lazyRowAdd(ModelMap model, @ModelAttribute("theWatchListDesc") WatchListDesc theWatchListDesc, 
		  @RequestParam("acc_id") Long accId) throws DAOException {
	logger.debug("Lazy-row loading for WatchListDesc::" + theWatchListDesc + "accid" + accId);
	List<String> temp = theWatchListDesc.getOperationParameterses().stream().map(o -> o.getName()).collect(Collectors.toList());
	
	if(temp.size() != temp.stream().distinct().count()) {
		model.addAttribute("error", "Please enter distinct symbols!");
		return "lazyRowLoad";
	}
    service.create(theWatchListDesc, accId);
    return "redirect:/admin/view-account?id="+accId;
  }
  
  @GetMapping("/delete-watchlist")
  public String deleteAccount(ModelMap model, @RequestParam("watchListId") Long watchListId, 
		  @RequestParam("accountId") Long accountId) {
	  logger.debug("Deleting watchlist for id::" + watchListId);
    try {
		service.delete(watchListId, accountId);
	} catch (DAOException e) {
		 logger.error("Error while deleting watchlist for id::" + e);
		e.printStackTrace();
	}
    return "redirect:/admin/view-account?id="+accountId;
  }
  
}
