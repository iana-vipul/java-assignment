package com.gjj.igden.dao.test.daostub;

import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.WatchListDesc;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class AccountDaoStub implements AccountDao {
  private static Map<Integer, Account> accountDbSimulator;

  public  AccountDaoStub(){
    accountDbSimulator = Maps.newHashMap(ImmutableMap
            .of(1, new Account(1L, "accountName_test1", "eMail_test1",
                            "additionalInfo_test1",
                            "password_test1", Stream.of(new WatchListDesc(), new WatchListDesc(),
                    new WatchListDesc(), new WatchListDesc()).collect(Collectors.toList()),
                            new Date()),
                    2, new Account(2L, "accountName_test2", "eMail_test2",
                            "additionalInfo_test2",
                            "password_test2", Stream.of(new WatchListDesc()).collect(Collectors.toList()),
                            new Date())));
  }


  public void setDataSource(DataSource dataSource) {
  }

  public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {
  }

  public List<Account> getAllAccounts() {
    //*/ return accountDbSimulator.entrySet().stream().collect(Collectors.toList());
    //**// return accountDbSimulator.values().stream().collect(Collectors.toList());
    //   return new ArrayList<>(accountDbSimulator.values());
    return accountDbSimulator.values()
      .stream()
      .filter(Objects::nonNull)
      .collect(Collectors.toList());
  }

  public boolean delete(Account account) {

    if (account !=null && account.getId() != null && account.getId() > 0) {
        accountDbSimulator.remove(account.getId().intValue(),account);
      return true;
    } else {
      return false;
    }

  }

  public boolean delete(Long id) {
    if (accountDbSimulator.get(id) != null) {
      accountDbSimulator.put(id.intValue(), null);
      return true;
    } else {
      return false;
    }
  }

  public Account getAccountById(Long id) {
    return accountDbSimulator.get(1);
  }

  public boolean update(Account acc) {
    return accountDbSimulator.put(acc.getId().intValue(), acc) == acc;
  }

  public boolean create(Account account) {
    // return accountDbSimulator.entrySet().stream().max((e1,e2)->e1.getKey())
    // return accountDbSimulator.Co().stream().max((e1,e2)->e1.getKey())
    Integer biggestKeyValue = Collections
      .max(accountDbSimulator.entrySet(), Map.Entry.comparingByKey()).getKey();
    account.setId(biggestKeyValue.longValue() + 1);
    return accountDbSimulator.put(++biggestKeyValue, account) == account;
  }

  @Override
  public boolean setImage(Long accId, InputStream is) {
    return true;
  }

  @Override
  public byte[] getImage(Long accId) {
    return new byte[0];
  }
}