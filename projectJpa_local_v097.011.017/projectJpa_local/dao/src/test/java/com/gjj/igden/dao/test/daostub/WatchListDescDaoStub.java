package com.gjj.igden.dao.test.daostub;

import com.gjj.igden.dao.WatchListDescDao;

import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@SuppressWarnings("unlikely-arg-type")
public class WatchListDescDaoStub implements WatchListDescDao {
  private static Map<Integer, List<IWatchListDesc>> watchListDescsDb;

  static {
    List<IWatchListDesc> watchListDescsAttachedToAccWithIdOne = Stream.of(new WatchListDesc(),
      new WatchListDesc(), new WatchListDesc(), new WatchListDesc(), new WatchListDesc(), new WatchListDesc(), new WatchListDesc(),
      new WatchListDesc(), new WatchListDesc())
            .collect(Collectors.toList());
    WatchListDesc theWatchListD = new WatchListDesc();
    theWatchListD.setWatchListName("test-aapl-5minBar-preMarketdata");

    String stock = "C@NYSE, GS@NYSE, C@NYSE, GS@NYSE, C@NYSE, GS@NYSE, C@NYSE, GS@NYSE, "+
            "C@NYSE, GS@NYSE, C@NYSE, GS@NYSE, C@NYSE, GS@NYSE, C@NYSE, GS@NYSE, C@NYSE, GS@NYSE";
    List<String> stockList = Arrays.asList(stock.split("\\s*,\\s*"));

    theWatchListD.setStockSymbolsList(stockList);

    watchListDescsAttachedToAccWithIdOne.get(1).setStockSymbolsList(stockList);

    List<IWatchListDesc> watchListDescsAttachedToAccWithIdTwo = Lists.newArrayList(theWatchListD);
    watchListDescsDb = Maps.newHashMap(ImmutableMap
      .of(1, watchListDescsAttachedToAccWithIdOne,
        2, watchListDescsAttachedToAccWithIdTwo));
  }

  @Override
  public List<String> getAllStockSymbols(Long id) {
    return watchListDescsDb.get(id.intValue()).get(0).getStockSymbolsList();
  }

  @Override
  public List<IWatchListDesc> getDataSetsAttachedToAcc(Integer id) {
    return watchListDescsDb.get(id);
  }

  @Override
  public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {
  }

@Override
  public IWatchListDesc getWatchListDesc(Long dsId, Long accId) {
     return watchListDescsDb.get(accId.intValue()).get(dsId.intValue());
  }

  @Override
  public boolean addTicker(Long watchlistId, String tickerName) {
    List<IWatchListDesc> watchListDescList = new ArrayList<>();
    WatchListDesc watchListDesc = new WatchListDesc();
    watchListDesc.setStockSymbolsList(new ArrayList<>());
    watchListDesc.getStockSymbolsList().add(tickerName);
    watchListDesc.getStockSymbolsList().add(tickerName);
    watchListDesc.getStockSymbolsList().add(tickerName);
    watchListDescList.add(watchListDesc);
    watchListDescsDb.put(watchlistId.intValue(), watchListDescList);
    return true;
  }

  @Override
  public boolean deleteWatchListDesc(Long dsId, Long accId) {  return true; }



  @Override
  public boolean deleteWatchListDesc(IWatchListDesc watchListDesc) {
    return watchListDescsDb.entrySet()
      .stream()
      .anyMatch(e -> e.getValue()
        .removeIf(p -> p.equals(watchListDesc)));
  }

@Override
  public boolean createWatchListDesc(IWatchListDesc watchListDesc) {
    return watchListDescsDb.get(watchListDesc.getWatchListId().intValue()).add(watchListDesc);
  }

  
@Override
  public boolean updateWatchListDesc(IWatchListDesc watchListDesc) {

    watchListDescsDb.get(watchListDesc.getWatchListId().intValue()).stream()
      .filter(a -> a.equals(watchListDesc))
      .findFirst()
      .ifPresent(p -> p.setWatchListName(watchListDesc.getWatchListName()));
    return true;
  }
}
