package com.gjj.igden.dao;

import java.util.List;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Bar;

public interface BarDao {
  Bar getSingleBar(long md_id, String instId);

  List<Bar> getBarList( String instId);

  boolean createBar(Bar bar) throws DAOException;

  boolean updateBar(Bar bar);

  boolean deleteBar(long md_id, String instId);

  boolean deleteBar(Bar bar);

  List<String> searchTickersByChars(String tickerNamePart);

  void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd);
}
