package com.gjj.igden.dao.daoimpl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.InstId;
import com.gjj.igden.model.InstIdKey;
import com.gjj.igden.model.WatchListDesc;

@Repository
@SuppressWarnings("unchecked")
public class InstIdDaoImpl extends AbstractDAO<InstId> {

	public void create(InstId instId) throws DAOException {
		super.create(instId);
	}
	@Override
	public InstId read(InstId obj) {
		//return (InstId) em.createQuery("from InstId where instId = '"+obj.getInstIdKey().getId()+"'").getSingleResult();
		return read(obj.getInstIdKey());
	}

	public InstId read(InstIdKey idKey) {
		return em.find(InstId.class, idKey);
	}
	
	@Override
	public List<InstId> readAll() {
		return em.createQuery("from InstId").getResultList();
	}
	
	public boolean delete(InstId instId) throws DAOException {
		em.remove(instId);
		return true;
	}
	
	public void delete(String instId) {
		em.createQuery("delete from InstId where instId='"+instId+"'");
	}
	
	public boolean delete(Long watchListId, Long accountId) {
	
		em.createNativeQuery("DELETE FROM wl_tickers WHERE account_fk_Id="+watchListId+" and watchlist_id="+accountId).executeUpdate();
		return true;
	}
	
	public void updateInstIdToNull(long watchListId_fk) {
		em.createQuery("update InstId set iWatchListDesc = 0 where watchlist_id_fk="+watchListId_fk).executeUpdate();
	}
	
	public List<String> getAllStockSymbols(WatchListDesc watchListDesc) {
		Long id = watchListDesc.getId();
		List<String> list = em.createNativeQuery("SELECT inst_id FROM wl_tickers WHERE watchlist_id = "+ id).getResultList();
		list.forEach(System.out::println);
		return list;
	}
	
	public List<String> searchTickersByChars(String tickerNamePart) {
		System.out.println("InstIdDaoImpl.searchTickersByChars():::::"+tickerNamePart);
		List<String> instIdList = em.createNativeQuery("Select instId_fk FROM market_data m where instId_fk LIKE '%"+tickerNamePart+"%'").getResultList();
		
		return instIdList;
	}
	
}
