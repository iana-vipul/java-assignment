package com.gjj.igden.dao;

import java.util.List;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.gjj.igden.model.IWatchListDesc;

@Repository
public interface WatchListDescDao {
	
	List<String> getAllStockSymbols(Long id);

	List<IWatchListDesc> getDataSetsAttachedToAcc(Integer id);

	void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd);

	IWatchListDesc getWatchListDesc(Long dsId, Long accId);

	boolean addTicker(Long watchlistId, String tickerName);

	boolean deleteWatchListDesc(Long dsId, Long accId);

	boolean deleteWatchListDesc(IWatchListDesc watchListDesc);

	boolean createWatchListDesc(IWatchListDesc watchListDesc);

	boolean updateWatchListDesc(IWatchListDesc watchListDesc);

}
