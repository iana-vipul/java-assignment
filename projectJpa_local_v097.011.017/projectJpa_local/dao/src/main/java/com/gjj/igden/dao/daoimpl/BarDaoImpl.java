package com.gjj.igden.dao.daoimpl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Bar;
import com.gjj.igden.model.BarKey;
import com.gjj.igden.model.InstId;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class BarDaoImpl extends AbstractDAO<Bar> {
  
	@Override
	public Bar read(Bar obj) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public List<Bar> readAll() {
		return em.createQuery("from Bar").getResultList();
	}

	/* I need to create new method because there is no method exist with matching argument type */
	public Bar getSingleBar(long mdId, String instId) {
		return (Bar) em.createQuery("FROM Bar b WHERE b.barKey.mdId = "+mdId+" AND b.barKey.instId LIKE '"+instId+"'").getSingleResult();
	}

	public Bar getSingleBar(BarKey barKey) {

		return (Bar) em.createQuery("FROM Bar b WHERE b.barKey.mdId = "+barKey.getMdId()+" AND b.barKey.instId LIKE '"+barKey.getInstId()+"'").getSingleResult();
	}
  
	public Set<Bar> getBarList(String instId) {
		List<Bar> bar1 = em.createQuery("FROM Bar WHERE instId_fk = '"+instId+"' ").getResultList();
		Set<Bar> bar2 = new HashSet<>();
		for(Bar bar:bar1) {
				bar2.add(bar);
		}
		return bar2;
	}
  
  
	public boolean createBar(Bar bar) throws DAOException {
		super.create(bar);
		return true;
	}
	
	public boolean updateBar(Bar bar) {
		super.update(bar);
		return true;
	}
	
	public boolean deleteBar(long mdId, String instId) {
		em.createQuery("DELETE FROM Bar b WHERE b.barKey.mdId = "+mdId+" AND b.barKey.instId LIKE '"+instId+"' ").executeUpdate();
		return true;
	}
	
	public boolean deleteBar(Bar bar) throws DAOException {
		super.delete(bar);
		return true;
	}
	
	public void deleteBarByInstId(InstId instId) {
		
	}
	
	public List<String> searchTickersByChars(String tickerNamePart) {
		List<Bar> bars = em.createQuery("FROM Bar b where b.barKey.instId LIKE '%"+tickerNamePart+"%'").getResultList();
		return bars.stream().map(b -> b.getBarKey().getInstId()).distinct().collect(Collectors.toList());
	}
	
/*	public List<String> searchTickersByChars(String tickerNamePart) {
		List<Bar> bars = em.createQuery("FROM Bar b where b.instId LIKE '%"+tickerNamePart+"%'").getResultList();
		return bars.stream().map(bar -> bar.getInstId().getInstId()).collect(Collectors.toList());
	}*/
	
	public List<Bar> getFullTickersObjectByChars(String tickerNamePart) {
		List<Bar> bars = em.createQuery("FROM Bar b where b.barKey.instId LIKE '%"+tickerNamePart+"%'").getResultList();
		bars.forEach(System.out::println);
		return bars;
	}
}
