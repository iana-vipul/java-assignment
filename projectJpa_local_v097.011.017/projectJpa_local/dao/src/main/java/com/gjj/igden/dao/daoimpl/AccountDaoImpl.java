package com.gjj.igden.dao.daoimpl;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Account;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class AccountDaoImpl extends AbstractDAO<Account> {
	
	@Autowired
	WatchListDescDaoImpl watchListDescDaoImpl;

    public void create(Account account) throws DAOException {
       em.merge(account);
    }

    @Override

    public Account read(Account account) {
        Account ac = (Account) em.createQuery("from Account where id = "+account.getId()).getSingleResult();
        return ac;
    }
    
	public Account readByAccountName(Account account) {
		Account ac;
		try {
			ac = (Account) em.createQuery("from Account where accountName LIKE '" + account.getAccountName() + "'")
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		return ac;
	}
	
	public Account readByAccountName(String accountName) {
		Account ac;
		try {
			ac = (Account) em.createQuery("from Account where accountName LIKE '" + accountName + "'")
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		return ac;
	}

    
	@Override
    public List<Account> readAll() {
        return em.createQuery("from Account").getResultList();
    }

    public boolean delete(Account account) throws DAOException {
    	System.out.println("AccountDaoImpl.delete()");
    	account = read(account);
    	//account.setAvatar(null);
    	account.setRoles(null);
    	return super.delete(account);
    	
    }

    public void update(Account account) {
    	super.update(account);
    }

    public boolean setImage(long accId, InputStream is) throws IOException {
		Account account = read(new Account(accId));
		byte [] bytes = IOUtils.toByteArray(is);
		is.close();
		account.setAvatar(bytes);
		update(account);
		return true;
	}

	public byte[] getImage(int accId) {
		Account account = em.find(Account.class, new Long(accId));
		return account.getAvatar();
	}
}