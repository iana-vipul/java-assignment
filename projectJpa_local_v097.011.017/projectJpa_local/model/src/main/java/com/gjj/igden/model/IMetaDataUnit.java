package com.gjj.igden.model;

import org.javatuples.Ennead;

import java.util.Date;

public interface IMetaDataUnit {

	Ennead<InstId, Long, Date, Integer, Double, Double, Double, Double, Long> getDataUnit();
}
