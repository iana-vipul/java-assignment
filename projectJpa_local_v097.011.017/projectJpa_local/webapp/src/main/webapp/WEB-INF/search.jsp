<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: denisigoshev
  Date: 6/15/2017
  Time: 4:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
<html>
<head>
  <title>Search data </title>
 <!--  <link href="css/bootstrap.css" rel="stylesheet" type="text/css"> -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"> </script>
 <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"> </script>
 <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <style type="text/css">
.horizontal1 {
	float: left;
	border: 1px solid green
}

.horizontal2 {
	float: right;
	border: 1px solid green
}

.ui-helper-hidden-accessible:hover {
	position: absolute;
	left: -9999px;
}

.ui-menu-item .ui-menu-item-wrapper {
	border: 1px;
}

.search {
	margin-left: 65px;
}

.top-margin {
	margin-top: 60px;
}

.table-margin {
	margin-top: -37px;
	margin-left: 325px;
	margin-right: -42px;
}

.search-box {
	width: 320px;
	margin-left: -75px;
	margin-right: -60px;
}
</style>
  <script>
  function ajaxcall(){
		var searchAjax = function(request, response) {
			var data = {}
			data["searchParam"] = request.term;
			$.ajax({
				type : "GET",
				contentType : "application/json;charset=utf-8",
				url : "${home}/admin/ajax_search?",
				data : data,
				dataType : 'json',
				processData : true,
				timeout : 100000,
				success : function(ajaxresponse) {
					console.log("SUCCESS: ", ajaxresponse);
					var instlist = ajaxresponse.item;
					response(instlist);
					console.log(instlist);
				},
				error : function(e) {
					console.log("ERROR--: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
		};

		var selectItem = function(event, ui) {
			$("#myText").val(ui.item.value);
			return false;
		}

		$("#myText")
				.autocomplete(
						{
							source : searchAjax,
							select : selectItem,
							minLength : 1,
							change : function() {
								$("#myText")
										.val(
												document
														.getElementById("myText").value);
							}
						});
}
		</script>
</head>
<h1>Let's make some search ! </h1>
<body>
<br>
<form action="DataController" method="get">
  <div class="container"   >
    <div class="form-group" style="width: 100%;" >
	      <div class="input-group">
	        
	          <input id="myText" onkeyup="ajaxcall();" name ="myText" class="search-box"
	                 placeholder="Type instrument id or first chars of id with '%' symbol"
	                 autocomplete="off">
	          <button type="submit" value="Search" class="btn btn-primary search">Search</button>
	
	      </div>
	      
		  <div class="table-margin" >
		    	<table border="1" class="table table-bordered table-striped" style="width: 100%;align-content: center;">
			    	<thead>
			    		<tr >
			    			<th>Instrument id</th>
			    			<th>Date-time</th>
			    			<th>Open</th>
			    			<th>High</th>
			    			<th>Low</th>
			    			<th>Close</th>
			    		</tr>
			    	</thead>
			    	<c:forEach var="bar" items="${THE_SEARCH_RESULT_LIST}">
			      		<tr align="center">
			        		<td> ${bar.barKey.instId} </td> 
			        		<td>${bar.dateTime}</td>
			        		<td> ${bar.open} </td> 
			        		<td> ${bar.high} </td> 
			        		<td> ${bar.low} </td> 
			        		<td> ${bar.close} </td> 
			      		</tr>
			      	</c:forEach>
			    </table>
		  </div>
	</div>
  </div>
</form>

</body>
</html>
